import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {default as v1} from './systemof-linear-equations/View';
import {default as v2} from './non-linear-systems/View';


import { Container, Nav, Navbar, NavDropdown, NavLink } from "react-bootstrap";
import { Route, BrowserRouter, Switch } from 'react-router-dom';


function Header() {
  return (
    <Navbar>
      <Container>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <NavLink href="/lab1">Lab 1</NavLink>
            <NavLink href="/lab2">Lab 2</NavLink>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>)
};

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <Switch>
          <Route path="/lab1" component={v1}/>
          <Route path="/lab2" component={v2}/>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;