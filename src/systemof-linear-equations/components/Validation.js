import { Alert, Button } from "react-bootstrap";

function validateMatrix(matrix) {
    var errors = [];

    if (matrix.length != 4 || matrix[0].length != 4) {
        errors.push("Размерность матрицы A должна быть = 4");
    }

    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (!/^-?\d+(\.\d+)*$/.test(matrix[i][j])) {
                errors.push("Матрица А должать содержать только цифры");
                return errors;
            }
        }
    }

    return errors;
};

function validateVector(vector) {
    var errors = [];

    if (vector.length != 4) {
        errors.push("Размерность матрицы D должна быть = 4");
    }

    for (let i = 0; i < vector.length; i++) {
        if (!/^-?\d+(\.\d+)*$/.test(vector[i])) {
            errors.push("Матрица D должать содержать только цифры");
            return errors;
        }
    }

    return errors;
};

function validateEps(eps) {
    var errors = [];
    if (eps >= 1 || eps < 0) {
        errors.push("Точность решения должна быть в пределах [0,1]");
    }
    if (!/^-?\d+(\.\d+)*$/.test(eps)) {
        errors.push("Точность должна быть числом");
    }
    return errors;
}

function Validation(props) {
    var errors = [...validateMatrix(props.matrixA),
    ...validateVector(props.matrixD),
    ...validateEps(props.eps)];

    return (
        <div className="validation">
            {errors.length != 0
                ? errors.map((error, key) => <Alert key={key} variant="danger">{error}</Alert>)
                : props.children}
        </div >
    );
}
export default Validation;