import React, { Component } from 'react';
import { Container, Nav, Navbar, DropdownButton, Dropdown, MenuItem, NavLink, InputGroup, FormControl } from "react-bootstrap";
import ReactInputMatrix from "react-input-matrix";
import Solution from './Solution';
import Comparison from './Compare';
import Solvers from '../solvers/Solvers'

class SolutionContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      matrix: [[]],
      matrixValid: false,
      matrixD: [],
      result: [],
      eps: 0.0001,
      selectedMethod: ""
    }
  }

  onAMatrixUpdate = (matrix) => {
    var newMatrix = [];

    for (let i = 0; i < matrix.length; i++) {
      newMatrix[i] = [];
      for (let j = 0; j < matrix[i].length; j++) {
        newMatrix[i][j] = matrix[i][j].value;
      }
    }
    this.setState({ matrix: newMatrix });
  }

  onDMatrixUpdate = (matrix) => {
    var newMatrix = [];

    for (let i = 0; i < matrix.length; i++) {
      for (let j = 0; j < matrix[i].length; j++) {
        newMatrix.push(matrix[i][j].value);
      }
    }
    this.setState({ matrixD: newMatrix });
  }

  onMethodSelectionChange = (val) => {
    this.setState({ selectedMethod: val });
  }

  onEpsChange = (val) => {
    this.setState({ eps: val.target.value })
  }

  render() {
    const solutionProps = {
      solver: Solvers.find(s => s.methodKey === this.state.selectedMethod)?.solver,
      matrixA: this.state.matrix,
      matrixD: this.state.matrixD,
      eps: this.state.eps
    };

    return (
      <Container>
        <h1>Лабораторная работа № 1</h1>
        <h2>Моделирование линейных систем</h2>
        <hr />
        <h3>Матрица A</h3>
        <ReactInputMatrix onMatrixChange={this.onAMatrixUpdate} />
        <h3>Матрица D</h3>
        <ReactInputMatrix onMatrixChange={this.onDMatrixUpdate} />
        <hr />
        <InputGroup className="mb-3">
          <InputGroup.Text>Точность</InputGroup.Text>
          <FormControl onChange={this.onEpsChange} value={this.state.eps} aria-label="Точность" />
          <DropdownButton title="Методы решения уравнений" onSelect={this.onMethodSelectionChange}>
            <Dropdown.Item eventKey="cramer">Метод Крамера</Dropdown.Item>
            <Dropdown.Item eventKey="gauss">Метод Гаусса</Dropdown.Item>
            <Dropdown.Item eventKey="seidel">Метод Зейделя</Dropdown.Item>
            <Dropdown.Item eventKey="iterative">Метод простой итерации</Dropdown.Item>
            <Dropdown.Divider />
            <Dropdown.Item eventKey="all">Решить всеми методами</Dropdown.Item>
          </DropdownButton>
        </InputGroup>
        <hr />
        {this.state.selectedMethod !== "all" && this.state.selectedMethod
          ? <Solution {...solutionProps} />
          : <Comparison
            matrixA={this.state.matrix}
            matrixD={this.state.matrixD}
            eps={this.state.eps}
          />}
      </Container>
    )
  }
}

export default SolutionContainer;