import React, { Component } from "react";
import { Alert, Button } from "react-bootstrap";
import Report from "./Report";
import Validation from "./Validation";

class Solution extends Component {
    constructor(props) {
        super(props);
        this.state = {
            result: []
        }
    }
    // меняется имя метода очищать репорт решения
    componentDidUpdate(prevProps, prevState) {
        if (this.props.solver != null && prevProps.solver != null
            && this.props.solver.methodKey != prevProps.solver.methodKey) {
            this.setState({ result: [] });
        }
    }

    solve = () => {
        const result = this.props.solver.solve(this.props.matrixA, this.props.matrixD, this.props.eps);
        this.setState({ result: result });
    }

    render() {
        return (
            <div>
                <Validation
                    matrixA={this.props.matrixA}
                    matrixD={this.props.matrixD}
                    eps={this.props.eps}>
                    <Button variant="success" onClick={this.solve}>Найти решение</Button>
                </Validation>
                <Report {...this.props}
                    result={this.state.result}
                />
            </div>
        )
    }
}

export default Solution;