import React, { Component } from "react";
import { Alert, Button, Table } from "react-bootstrap";
import MethodsDescription from "../../methodsDescription";
import Solvers from "../solvers/Solvers";
import Report from "./Report";
import Validation from "./Validation";
// с помощтю ts задать ограницение на пропсы, чтобы жестко ограничить возможные методы решения слау
// Добавить вторую матрицу с максимом столбцов = 1

// TODO
/*
    отрефакторить таблицы
    перенести округление в солверы
    перепроверить методы зайделя и простых итерации - большая ошибка
*/
function round(value, eps) {
    var round = epsNumber(eps);
    var m = Math.pow(10, round);
    return Math.round(value * m) / m;
};

function epsNumber(eps) {
    let i = 0;
    if (eps % 1 != 0) {
        do {
            eps = eps * 10;
            i++;
        } while (eps % 1 != 0);
    }
    return i;
}

function ComparisonTable({ results }) {
    return (
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th></th>
                    <th>{MethodsDescription.gauss.methodName}</th>
                    <th>{MethodsDescription.cramer.methodName}</th>
                    <th>{MethodsDescription.iterative.methodName}</th>
                    <th>{MethodsDescription.seidel.methodName}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>X1</th>
                    <td>{results.gauss.x[0]}</td>
                    <td>{results.cramer.x[0]}</td>
                    <td>{results.iterative.x[0]}</td>
                    <td>{results.seidel.x[0]}</td>
                </tr>
                <tr>
                    <th>X2</th>
                    <td>{results.gauss.x[1]}</td>
                    <td>{results.cramer.x[1]}</td>
                    <td>{results.iterative.x[1]}</td>
                    <td>{results.seidel.x[1]}</td>
                </tr>
                <tr>
                    <th>X3</th>
                    <td>{results.gauss.x[2]}</td>
                    <td>{results.cramer.x[2]}</td>
                    <td>{results.iterative.x[2]}</td>
                    <td>{results.seidel.x[2]}</td>
                </tr>
                <tr>
                    <th>X4</th>
                    <td>{results.gauss.x[3]}</td>
                    <td>{results.cramer.x[3]}</td>
                    <td>{results.iterative.x[3]}</td>
                    <td>{results.seidel.x[3]}</td>
                </tr>
            </tbody>
        </Table>
    )
}

function DiffTable({ results }) {
    const rows = (index) => (
        <>
            <th>{results.matrixD[index]}</th>
            <td>{results.gauss.diff[index]}</td>
            <td className="diff">{round((results.gauss.diff[index] - results.matrixD[index]) / results.matrixD[index] * 100, results.eps)}%</td>
            <td>{results.cramer.diff[index]}</td>
            <td className="diff">{round((results.cramer.diff[index] - results.matrixD[index]) / results.matrixD[index] * 100, results.eps)}%</td>
            <td>{results.iterative.diff[index]}</td>
            <td className="diff">{round((results.iterative.diff[index] - results.matrixD[index]) / results.matrixD[index] * 100, results.eps)}%</td>
            <td>{results.seidel.diff[index]}</td>
            <td className="diff">{round((results.seidel.diff[index] - results.matrixD[index]) / results.matrixD[index] * 100, results.eps)}%</td>
        </>
    )
    return (
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th></th>
                    <th>Исходное значение</th>
                    <th colSpan="2">{MethodsDescription.gauss.methodName}</th>
                    <th colSpan="2">{MethodsDescription.cramer.methodName}</th>
                    <th colSpan="2">{MethodsDescription.iterative.methodName}</th>
                    <th colSpan="2">{MethodsDescription.seidel.methodName}</th>
                </tr>
                <tr>
                    <th>b1</th>
                    {rows(0)}
                </tr>
                <tr>
                    <th>b2</th>
                    {rows(1)}
                </tr>
                <tr>
                    <th>b3</th>
                    {rows(2)}
                </tr>
                <tr>
                    <th>b4</th>
                    {rows(3)}
                </tr>
            </thead>
        </Table>
    )
}

function CalculateDiff(matrixA, matrixD, matrixX, eps) {
    var actualD = [0, 0, 0, 0];
    for (let i = 0; i < matrixA.length; i++) {
        for (let j = 0; j < matrixA[0].length; j++) {
            actualD[i] += matrixA[i][j] * matrixX[j];
        }
    }
    return actualD.map(a => round(a, eps));
}

class Comparison extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cramer: {
                x: [],
                diff: []
            },
            gauss: {
                x: [],
                diff: []
            },
            iterative: {
                x: [],
                diff: []
            },
            seidel: {
                x: [],
                diff: []
            },
        }
    }

    solve = () => {
        const data = [this.props.matrixA, this.props.matrixD, this.props.eps];

        var cramer = Solvers.find(s => s.methodKey == "cramer").solver.solve(...data);
        var gauss = Solvers.find(s => s.methodKey == "gauss").solver.solve(...data);
        var iterative = Solvers.find(s => s.methodKey == "iterative").solver.solve(...data);
        var seidel = Solvers.find(s => s.methodKey == "seidel").solver.solve(...data);

        this.setState(
            {
                cramer: { x: cramer, diff: CalculateDiff(this.props.matrixA, this.props.matrixD, cramer, this.props.eps) },
                gauss: { x: gauss, diff: CalculateDiff(this.props.matrixA, this.props.matrixD, gauss, this.props.eps) },
                iterative: { x: iterative, diff: CalculateDiff(this.props.matrixA, this.props.matrixD, iterative, this.props.eps) },
                seidel: { x: seidel, diff: CalculateDiff(this.props.matrixA, this.props.matrixD, seidel, this.props.eps) },
            })
    }

    render() {
        const results = {
            ...this.state,
            matrixD: this.props.matrixD,
            eps: this.props.eps
        };

        return (
            <div>
                <Validation matrixA={this.props.matrixA}
                    matrixD={this.props.matrixD}
                    eps={this.props.eps}>
                    <Button variant="success" onClick={this.solve}>Найти решение</Button>
                    <hr />
                    <h3>Решение</h3>
                    <ComparisonTable results={this.state} />
                    <h3>Сравнение погрешностей</h3>
                    <DiffTable results={results} />
                </Validation>
            </div>
        )
    }
}

export default Comparison;