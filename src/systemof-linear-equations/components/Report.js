import Latex from "react-latex";

function Report(props) {
    var lines = [];
    props.matrixA.forEach(arr => {
        lines.push(arr.join(" & "));
    })
    return (
        <div className="report">
            {/* метод*/}

            <Latex displayMode={true}>{`$$\{\\huge${props.solver.methodName.replaceAll(" ", "\\space")}\}$$`}</Latex>

            {/* описание*/}

            <Latex displayMode={true}>{`$$${props.solver.description.replaceAll(" ", "\\space")}$$`}</Latex>

            {/* матрица А*/}

            <Latex displayMode={true}>{`$$
                \\ Матрица\\ A =
                \\left( 
                \\begin{array}{cc|c}
                    ${lines.join('\\\\')}
                \\end{array}
                \\right)
                $$`}
            </Latex>

            {/* вектор D*/}

            <Latex displayMode={true}>{`$$
                \\ Матрица\\ D =
                \\left(
                \\begin{array}{cc|c}
                    ${props.matrixD != null && props.matrixD.join(" & ")}
                \\end{array}
                \\right)
                $$`}
            </Latex>

            {/* решение*/}

            <Latex displayMode={true}>{`$$
                \\ Решение
                \\left(
                \\begin{array}{cc|c}
                    ${props.result != null && props.result.join(" & ")}
                \\end{array}
                \\right)
                $$`}
            </Latex>
        </div>
    );
}
export default Report;