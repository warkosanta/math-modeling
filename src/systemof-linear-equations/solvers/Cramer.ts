import { det } from "mathjs";
import Solver from "./Solver";

class CramerSolver extends Solver {
    solve(matrixA: number[][], matrixD: number[], eps: number): number[] {

        var detA = det(matrixA);

        if (detA === 0) {
            throw new Error("Определитель равен нулю");
        }

        var A1 = this.changeCol(matrixA, matrixD, 0);
        var A2 = this.changeCol(matrixA, matrixD, 1);
        var A3 = this.changeCol(matrixA, matrixD, 2);
        var A4 = this.changeCol(matrixA, matrixD, 3);

        var det1 = det(A1);
        var det2 = det(A2);
        var det3 = det(A3);
        var det4 = det(A4);

        return [
            this.round(det1 / detA, eps),
            this.round(det2 / detA, eps),
            this.round(det3 / detA, eps),
            this.round(det4 / detA, eps)
        ];
    }


    private changeCol(matrixA: number[][], matrixD: number[], row: number) {
        var newA = matrixA.map(function (arr) {
            return arr.slice();
        });

        for (let index = 0; index < matrixA.length; index++) {
            newA[index][row] = matrixD[index];
        }
        return newA;
    }
}
export default CramerSolver;

