import Solver from "./Solver";

class GaussSolver extends Solver {

    private initial_a_matrix: number[][] = [];
    private a_matrix: number[][] = [];
    private x_vector: number[] = [];
    private initial_b_vector: number[] = [];
    private b_vector: number[] = [];
    private u_vector: number[] = []
    private eps: number = 0;
    private size: number = 0;

    solve(matrixA: number[][], matrixD: number[], eps: number): number[] {

        if (matrixA === null || matrixD === null) throw new Error();

        var b_length = matrixD.length;
        var a_length = [matrixA.length, matrixA[0].length].reduce((a, b) => a * b);

        if (a_length !== b_length * b_length) throw new Error();

        this.initial_a_matrix = matrixA;
        this.a_matrix = matrixA.map(function (arr) {
            return arr.slice();
        });;

        this.initial_b_vector = matrixD;
        this.b_vector = matrixD.slice();
        this.size = b_length;
        this.eps = eps;

        this.GaussSolve();

        return this.x_vector.map(x => this.round(x, eps));
    }

    private GaussSolve() {
        var index = this.InitIndex();
        this.GaussForwardStroke(index);
        this.GaussBackwardStroke(index);
        this.GaussDiscrepancy();
    }

    private FindR(row: number, index: number[]) {
        var max_index = row;
        var max = this.a_matrix[row][index[max_index]];
        var max_abs = Math.abs(max);
        for (let cur_index = row + 1; cur_index < this.size; ++cur_index) {
            var cur = this.a_matrix[row][index[cur_index]];
            var cur_abs = Math.abs(cur);
            if (cur_abs > max_abs) {
                max_index = cur_index;
                max = cur;
                max_abs = cur_abs;
            }
        }

        if (max_abs < this.eps) {

            console.log('err');
            return -1;

        }

        var temp = index[row];
        index[row] = index[max_index];
        index[max_index] = temp;

        return max;
    }

    private InitIndex() {
        var index = [];
        for (let i = 0; i < this.size; ++i)
            index[i] = i;
        return index;
    }


    private GaussForwardStroke(index: number[]) {
        for (let i = 0; i < this.size; ++i) {
            var r = this.FindR(i, index);

            for (let j = 0; j < this.size; ++j)
                this.a_matrix[i][j] /= r;

            this.b_vector[i] /= r;

            for (let k = i + 1; k < this.size; ++k) {
                var p = this.a_matrix[k][index[i]];
                for (let j = i; j < this.size; ++j)
                    this.a_matrix[k][index[j]] -= this.a_matrix[i][index[j]] * p;
                this.b_vector[k] -= this.b_vector[i] * p;
                this.a_matrix[k][index[i]] = 0.0;
            }
        }
    }

    private GaussBackwardStroke(index: number[]) {
        for (let i = this.size - 1; i >= 0; --i) {
            var x_i = this.b_vector[i];
            for (let j = i + 1; j < this.size; ++j)
                x_i -= this.x_vector[index[j]] * this.a_matrix[i][index[j]];
            this.x_vector[index[i]] = x_i;
        }
    }

    private GaussDiscrepancy() {
        for (let i = 0; i < this.size; ++i) {
            var actual_b_i = 0.0;
            for (let j = 0; j < this.size; ++j)
                actual_b_i += this.initial_a_matrix[i][j] * this.x_vector[j];
            this.u_vector[i] = this.initial_b_vector[i] - actual_b_i;
        }
    }
}

export default GaussSolver;