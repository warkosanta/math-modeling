
abstract class Solver {

    methodKey: string;
    methodName: string;
    description: string;

    constructor(methodKey: string, methodName: string, description: string) {
        this.methodKey = methodKey;
        this.methodName = methodName;
        this.description = description;
    }

    abstract solve(matrixA: number[][], matrixD: number[], eps: number): number[];

    protected round(value: number, eps: number): number {
        if (isNaN(value) || isNaN(eps)) throw new TypeError();
        var round = this.epsNumber(eps);
        var m = Math.pow(10, round);
        return Math.round(value * m) / m;
    }

    protected isMatrixConvergent(matrixB: number[][]): number {
        var l = matrixB.length;
        var C = [0, 0, 0, 0];
        for (let i = 0; i < l; i++) {
            for (let j = 0; j < l; j++) {
                C[i] += Math.abs(matrixB[i][j]);
            }
        }
        try {
            if (Math.max(...C) >= 1) {
                throw new Error();
            }
        }
        catch {
            console.log();
        }
        return Math.max(...C);
    }

    protected makeAbs(numbers: number[]): number[] {
        var x = [];
        for (let index = 0; index < numbers.length; index++) {
            x[index] = Math.abs(numbers[index]);
        }
        return x;
    }

    private epsNumber(eps: number): number {
        let i = 0;
        if (eps % 1 != 0) {
            do {
                eps = eps * 10;
                i++;
            } while (eps % 1 != 0);
        }
        return i;
    }
}

export default Solver;
