import Solver from "./Solver";

class SeidelSolver extends Solver {
    solve(matrixA: number[][], matrixD: number[], eps: number): number[] {
        var B: number[][] = [[]]

        var l = matrixA.length;
        var e = [];
        var X: number[][] = [[]]

        for (let i = 0; i < l; i++) {
            B[i] = [];
            for (let j = 0; j < l; j++) {
                B[i][j] = 0;
            }
        }

        for (let i = 0; i < l; i++) {
            for (let j = 0; j < l; j++) {
                B[i][j] = -(matrixA[i][j] / matrixA[i][i]);
                B[i][i] = 0;
            }
            e[i] = matrixD[i] / matrixA[i][i];
        }
        var normE = Math.max(...this.makeAbs(e));

        var C = this.isMatrixConvergent(B);
        var K = Math.abs(Math.ceil((1 / Math.log10(C)) * (Math.log10(normE)
            - Math.log10(eps) - Math.log10((1 - C))) - 1));
        for (let i = 0; i < K; i++) {
            X[i] = [];
            for (let j = 0; j < l; j++) {
                X[i][j] = 0;
            }
        }
        X[0] = e;
        var V: number[][] = [];

        for (let k = 0; k < K; k++) {
            V[k] = [];
            for (let i = 0; i < l; i++) {
                V[k][i] = 0;

                if (0 === i) {
                    for (let j = 1; j < l; j++) {
                        V[k][i] += B[i][j] * X[k][j];
                    }
                } else {
                    for (let j = 0; j < i; j++) {
                        V[k][i] += B[i][j] * X[k + 1][j];
                    }
                    for (let j = i + 1; j < l; j++) {
                        V[k][i] += B[i][j] * X[k][j];
                    }
                }

                try {
                    X[k + 1][i] = e[i] + V[k][i];
                }
                catch {
                    return X[k].map(x => this.round(x, eps));
                }
            }
        }
        return X[0].map(x => this.round(x, eps));
    }
}
export default SeidelSolver;