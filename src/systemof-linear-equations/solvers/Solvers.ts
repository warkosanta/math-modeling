import IterativeSolver from "./Iterative";
import SeidelSolver from "./Seidel";
import GaussSolver from "./Gauss";
import CramerSolver from "./Cramer";
import Solver from "./Solver";
import MethodsContent from "../../methodsDescription";

enum MethodNames {
  Cramer = "cramer",
  Iterative = "iterative",
  Seidel = "seidel",
  Gauss = "gauss",
}

function getMethodContent(methodName: MethodNames): [string, string, string] {
  return [
    MethodsContent[methodName].methodKey,
    MethodsContent[methodName].methodName,
    MethodsContent[methodName].description,
  ]
}

const Solvers: { methodKey: MethodNames, solver: Solver }[] = [
  { methodKey: MethodNames.Cramer, solver: new CramerSolver(...getMethodContent(MethodNames.Cramer)) },
  { methodKey: MethodNames.Iterative, solver: new IterativeSolver(...getMethodContent(MethodNames.Iterative)) },
  { methodKey: MethodNames.Seidel, solver: new SeidelSolver(...getMethodContent(MethodNames.Seidel)) },
  { methodKey: MethodNames.Gauss, solver: new GaussSolver(...getMethodContent(MethodNames.Gauss)) }
];

export default Solvers;