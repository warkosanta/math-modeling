import math from "mathjs";

const systemEquation = {
  var23: {
    f1: (x: number, y: number) => Math.cos(x) + y - 1.5,
    f2: (x: number, y: number) => 2 * x - Math.sin(y - 0.5) - 1,

    f1_get_y: (x: number) => 1.5 - Math.cos(x),
    f2_get_x: (y: number) => (1 + Math.sin(y - 0.5)) / 2,

    f1_der_x: (x: number, y: number) => -Math.sin(x),
    f1_der_y: (x: number, y: number) => 1,
    f2_der_x: (x: number, y: number) => 2,
    f2_der_y: (x: number, y: number) => -Math.cos(y - 0.5),

    check_f1: (x: number, y: number) => Math.cos(x) + y,
    check_f2: (x: number, y: number) => 2 * x - Math.sin(y - 0.5),
  },
  var8: {
    f1: (x: number, y: number) => Math.log10(x + 2) - y - 1,
    f2: (x: number, y: number) => x * x + 2 * y * y - 0.9,

    f1_get_y: (x: number) => Math.log10(x + 2) - 1,
    f2_get_x: (y: number) => Math.sqrt(Math.abs(0.9-2*y*y)),

    f2_der_x: (x: number, y: number) => 2*x,
    f2_der_y: (x: number, y: number) => 4*y,

    f1_der_x: (x: number, y: number) => 1/(x+2),
    f1_der_y: (x: number, y: number) => -1,

    check_f1: (x: number, y: number) => Math.log10(x + 2) - y,
    check_f2: (x: number, y: number) => Math.abs(x * x - 2 * y * y),
  },
  var12: {
    f1: (x: number, y: number) => -Math.sin(x + 1) + y - 0.8,
    f2: (x: number, y: number) => Math.sin(y - 1) - x - 1.3,

    f1_get_y: (x: number) => 0.8 + Math.sin(x + 1),
    f2_get_x: (y: number) => Math.sin(y - 1) - 1.3,
    f1_der_x: (x: number, y: number) => -Math.cos(x + 1),
    f1_der_y: (x: number, y: number) => 1,
    f2_der_x: (x: number, y: number) => -1,
    f2_der_y: (x: number, y: number) => Math.cos(y - 1),

    check_f1: (x: number, y: number) => -Math.sin(x + 1) + y,
    check_f2: (x: number, y: number) => Math.sin(y - 1) - x,
  },
};

export default systemEquation;
