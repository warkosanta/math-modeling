import func from "./func";
import ISolver from "./Solver";

class Bisection implements ISolver {
    Solve(eps: number, equations: any, a: number, b: number, x0?: number): number {
        var c = 0;
        while (b - a > eps) {
            c = (a + b) / 2;
            if (equations.equation(b) * equations.equation(c) < 0) {
                a = c;
            } else {
                b = c;
            }
        }
        return c;
    }
}

export default Bisection;