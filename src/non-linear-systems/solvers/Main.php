<?php

/**
 * Created by PhpStorm.
 * User: harmakit
 * Date: 30/09/2018
 * Time: 17:53
 */
include_once "Bisection.php";
include_once "Chords.php";
include_once "Newton.php";
include_once "Combined.php";
include_once "NewtonSystem.php";
include_once "IterationSystem.php";
include_once "SeidelSystem.php";

$eps = 0.0001;

print("Вычисление нелинейного уравнения\n1 - Метод половиннного деления\n2 - Метод хорд\n3 - Метод касательных (Ньютона)\n4 - Комбинированный метод\n");
print("Вычисление системы нелинейных уравнений\n5 - Метод Ньютона\n6 - Метод простой итерации\n7 - Метод Зейделя\n9 - Изменить точность\n0 - Выход");
$input = (int) readline("\nВыберите пункт меню: ");

$work = true;
$bisection = new Bisection();
$chords = new Chords();
$combined = new Combined();
$newton = new Newton();

$newtonSystem = new NewtonSystem();
$iterationSystem = new IterationSystem();
$seidelSystem = new SeidelSystem();

while ($work) {
    switch ($input) {
        case 1:
            $bisection->setA((double) readline("\nВведите a: "));
            $bisection->setB((double) readline("\nВведите b: "));
            $bisection->setEps($eps);
            $answer = $bisection->solve();
            print ("x = $answer; ");
            print(PHP_EOL);
            print ("e = " . func::process($answer) . "; ");
            print(PHP_EOL);
            break;
        case 2:
            $chords->setA((double) readline("\nВведите a: "));
            $chords->setB((double) readline("\nВведите b: "));
            $chords->setEps($eps);
            $answer = $chords->solve();
            print ("x = $answer; ");
            print(PHP_EOL);
            print ("e = " . func::process($answer) . "; ");
            print(PHP_EOL);
            break;
        case 3:
            $newton->setX0((double) readline("\nВведите x0: "));
            $newton->setEps($eps);
            $answer = $newton->solve();
            print ("x = $answer; ");
            print(PHP_EOL);
            print ("e = " . func::process($answer) . "; ");
            print(PHP_EOL);
            break;
        case 4:
            $combined->setA((double) readline("\nВведите a: "));
            $combined->setB((double) readline("\nВведите b: "));
            $combined->setEps($eps);
            $answer = $combined->solve();
            print ("x = $answer; ");
            print(PHP_EOL);
            print ("e = " . func::process($answer) . "; ");
            print(PHP_EOL);
            break;
        case 5:
            $newtonSystem->setEps($eps);
            $answers = $newtonSystem->solve();
            print ("x = $answers[0]; y = $answers[1]; i = $answers[2]");
            print(PHP_EOL);
            print ("e = " . system::f1($answers[0], $answers[1]) . "; ");
            print(PHP_EOL);
            break;
        case 6:
            $iterationSystem->setEps($eps);
            $answers = $iterationSystem->solve();
            print ("x = $answers[0]; y = $answers[1]; i = $answers[2]");
            print(PHP_EOL);
            print ("e = " . system::f1($answers[0], $answers[1]) . "; ");
            print(PHP_EOL);
            break;
        case 7:
            $seidelSystem->setEps($eps);
            $answers = $seidelSystem->solve();
            print ("x = $answers[0]; y = $answers[1]; i = $answers[2]");
            print(PHP_EOL);
            print ("e = " . system::f1($answers[0], $answers[1]) . "; ");
            print(PHP_EOL);
            break;
        case 9:
            $eps = (double) readline("\nВведите точность: ");
            break;
        default:
            $work = false;
            break;
    }
    if ($work) {
        $input = (int) readline("\nВыберите пункт меню: ");
    }
}

