import ISolver from "./Solver";

class Combined implements ISolver {
    Solve(eps: number, equations: any, a: number, b: number, x0?: number): number {
        while (Math.abs(a - b) > 2 * eps) {
            if (equations.equation(a) * equations.derivative2(a) < 0) {
                a = a - equations.equation(a) * ((a - b) / (equations.equation(a) - equations.equation(b)));
            } else if (equations.equation(a) * equations.derivative2(a) > 0) {
                a = a - (equations.equation(a) / equations.derivative(a));
            }
            if (equations.equation(b) * equations.derivative2(b) < 0) {
                b = b - equations.equation(b) * ((b - a) / (equations.equation(b) - equations.equation(a)));
            } else if (equations.equation(b) * equations.derivative2(b) > 0) {
                b = b - (equations.equation(b) / equations.derivative(b));
            }
        }
        return (a + b) / 2;
    }
}

export default Combined;