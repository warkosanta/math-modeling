import ISolver from "./Solver";

class Chords implements ISolver {
    Solve(eps: number, equations: any, a: number, b: number, x0?: number): number {
        while (Math.abs(b - a) > eps) {
            a = b - (b - a) * equations.equation(b) / (equations.equation(b) - equations.equation(a));
            b = a + (a - b) * equations.equation(a) / (equations.equation(a) - equations.equation(b));
        }
        return b;
    }
}

export default Chords;