import equations from "./equations";
import ISolver from "./Solver";

class Newton implements ISolver {
    Solve(eps: number, equations: any, x0: number, a?: number, b?: number): number {

        var x1 = x0 - equations.equation(x0) / equations.derivative(x0);

        while (Math.abs(x1 - x0) > eps) {
            x0 = x1;
            x1 = x1 - equations.equation(x1) / equations.derivative(x1);
        }
        return x1;
    }
}

export default Newton;