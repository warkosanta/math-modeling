import ISolver from "./Solver";

class IterationSystem implements ISolver {
    Solve(eps: number, equations: any, a?: number, b?: number, x0?: number): any {

        var i = 0;
        var x = 0;
        var y = 0;

        do {
            i++;

            var x_sled = equations.f2_get_x(y);
            var y_sled = equations.f1_get_y(x);

            if (Math.abs(x_sled - x) <= eps && Math.abs(y_sled - y) <= eps) {
                break;
            }
            x = x_sled;
            y = y_sled;

        } while (i < 20);

        return [
            x, y, i
        ];
    }
}

export default IterationSystem;


//            J = system::f1_der_x(x, y) * system::f2_der_y(x, y) - system::f1_der_y(x, y) * system::f2_der_x(x, y);
//
//            x_sled = x + (
//                (-system::f1(x, y) * system::f2_der_y(x, y))
//                    -
//                (-system::f2(x, y) * system::f1_der_y(x, y))
//            ) / J;
//
//            y_sled = x + (
//                (-system::f2(x, y) * system::f1_der_x(x, y))
//                    -
//                (-system::f1(x, y) * system::f2_der_x(x, y))
//            ) / J;

