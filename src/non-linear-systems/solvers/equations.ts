const equations = {
    var23: {
        equation: (x: number) => Math.log10(x) - Math.cos(x),
        derivative: (x: number) => Math.sin(x) + 1 / x,
        derivative2: (x: number) => Math.cos(x) + 1 / x ** 2
    },
    var8: {
        equation: (x: number) => (x+1) ** 3 - x,
        derivative: (x: number) => 3 * (x+1) ** 2 - 1,
        derivative2: (x: number) => 6 * (x+1)
    },
    var12: {
        equation: (x: number) => Math.cbrt(5 - x) - x,
        derivative: (x: number) => -1 - 1 / (3 * (5 - x) ** (2 / 3)),
        derivative2: (x: number) => - 2 / (9 * (5 - x) ** (5 / 3))
    }
}

export default equations;