import ISolver from "./Solver";

class SeidelSystem implements ISolver {
    Solve(eps: number, equations: any, a?: number, b?: number, x0?: number) {
        var i = 0;
        var x = 0;
        var y = 0;

        do {
            i++;

            var x_sled = equations.f2_get_x(y);
            var y_sled = equations.f1_get_y(x_sled);

            if (Math.abs(x_sled - x) <= eps && Math.abs(y_sled - y) <= eps) {
                break;
            }
            x = x_sled;
            y = y_sled;

        } while (i < 20);

        return [
            x, y, i
        ];

    }

}

export default SeidelSystem;