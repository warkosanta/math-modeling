import ISolver from "./Solver";

class NewtonSystem implements ISolver {
    Solve(eps: number, equations: any, a?: number, b?: number, x0?: number) {

        var i = 0;
        var x = 1;
        var y = 1;

        do {
            i++;
            var temp: number[][] = [[],[]];
            temp[0][0] = equations.f1_der_x(x, y);
            temp[0][1] = equations.f1_der_y(x, y);
            temp[1][0] = equations.f2_der_x(x, y);
            temp[1][1] = equations.f2_der_y(x, y);
            temp = this.ober_matr(temp);

            var dx = -temp[0][0] * equations.f1(x, y) + -temp[0][1] * equations.f2(x, y);
            var dy = -temp[1][0] * equations.f1(x, y) + -temp[1][1] * equations.f2(x, y);
            x = x + dx;
            y = y + dy;

            var tempB: number[] = [];
            tempB[0] = equations.f1(x, y);
            tempB[1] = equations.f2(x, y);
            var norm = Math.sqrt(tempB[0] * tempB[0] + tempB[1] * tempB[1]);
            i++;

        } while (norm > eps && i < 20);

        return [
            x, y, i
        ];

    }

    private ober_matr(temp: number[][]) {
        var det = temp[0][0] * temp[1][1] - temp[0][1] * temp[1][0];
        var aa = temp[0][0];
        temp[0][0] = temp[1][1] / det;
        temp[1][1] = aa / det;
        temp[0][1] = -temp[0][1] / det;
        temp[1][0] = -temp[1][0] / det;
        return temp;
    }

}

export default NewtonSystem;