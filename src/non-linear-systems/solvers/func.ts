class func {
    public static process = (x: number) => x ** 4 - 3 * x ** 2 + 4 * x - 1;
    public static derivative = (x: number) => 4 * x ** 3 - 6 * x + 4; // первая производная
    public static derivative2 = (x: number) => 12 * x ** 2 - 6; // вторая производная
}

export default func;