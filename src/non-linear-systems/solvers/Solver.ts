interface ISolver {
    Solve(eps: number, equations: any, a?: number, b?: number, x0?: number): any;
}

abstract class Solver {

    protected _a: number = 0;
    protected _b: number = 0;
    protected _eps: number = 0;

    setA(a: number) {
        this._a = a;
    }

    setB(b: number) {
        this._b = b;
    }

    setEps(eps: number) {
        this._eps = eps;
    }

    abstract Solve(a: number, b: number, eps: number): number;
}

export default ISolver;