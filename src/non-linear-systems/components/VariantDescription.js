import Latex from "react-latex";

const variants = {
    var23: {
        title: "Вариант 23",
        equation: "lg{x}-cosx=0",
        system1: "cosx+y=1.5",
        system2: "2*x-sin(y-0.5)=1"
    },
    var8: {
        title: "Вариант 8",
        equation: "(x+1)^3-x=0",
        system1: "lg{(x+2)}-y=1",
        system2: "x^2+y^2=0.9"
    },
    var12: {
        title: "Вариант 12",
        equation: "x=\\sqrt[3](5-x)",
        system1: "-sin(x+1)+y=0.8",
        system2: "sin(y-1)-x=1.3"
    }
}


function VariantDescription(props) {
    const variant = variants[props.variant];
    return (
        <div className="report">
            <Latex displayMode={true}>{`$$\{\\huge${variant.title.replaceAll(' ', "\\space")}\}$$`}</Latex>

            {/* линейное уравнение */}
            <Latex displayMode={true}>{`$$Линейное\\spaceуравнение:\\space ${variant.equation}$$`}</Latex>

            {/* система уравнений */}
            <Latex displayMode={true}>{`$$Система\\spaceуравнений:$$`}</Latex>

            <Latex displayMode={true}>{`$$
                \\begin{cases}
                ${variant.system1}\\\\
                ${variant.system2}
                \\end{cases}
                $$`}
            </Latex>
        </div>
    );
}
export default VariantDescription;