import React, { Component } from "react";
import { Table } from "react-bootstrap";
import Bisection from "../solvers/Bisection";
import Chords from "../solvers/Chords";
import Combined from "../solvers/Combined";
import IterationSystem from "../solvers/IterationSystem";
import Newton from "../solvers/Newton";
import NewtonSystem from "../solvers/NewtonSystem";
import SeidelSystem from "../solvers/SeidelSystem";

class SingleEquationTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            variant: ""
        }

        this.bisectionSolver = new Bisection();
        this.chordSolver = new Chords();
        this.combinedSolver = new Combined();

        this.newtonSolver = new Newton();

        this.newtonSystemSolver = new NewtonSystem();
        this.iterationSolver = new IterationSystem();
        this.seidalSolver = new SeidelSystem();
    }
    render() {
        const input = [
            this.props.eps,
            this.props.equations,
            this.props.a,
            this.props.b,
        ];

        const systemOutput = [
            this.seidalSolver.Solve(this.props.eps, this.props.systemEquation),
            this.iterationSolver.Solve(this.props.eps, this.props.systemEquation),
            this.newtonSystemSolver.Solve(this.props.eps, this.props.systemEquation)
        ];

        return (
            <div>
                <h3>Решение нелинейное уравнения</h3>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Метод половинного деления</th>
                            <th>Метод хорд</th>
                            <th>Метод касательных (Ньютона)</th>
                            <th>Комбинированный метод</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>X</th>
                            <td>{this.bisectionSolver.Solve(...input)}</td>
                            <td>{this.chordSolver.Solve(...input)}</td>
                            <td>{this.newtonSolver.Solve(this.props.eps, this.props.equations, this.props.x0)}</td>
                            <td>{this.combinedSolver.Solve(...input)}</td>
                        </tr>
                    </tbody>
                </Table>
                <h3>Решение системы нелинейных уравнений</h3>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Метод Зейделя</th>
                            <th>Метод простой итерации</th>
                            <th>Метод касательных (Ньютона)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>X</th>
                            <td>{systemOutput[0][0]}</td>
                            <td>{systemOutput[1][0]}</td>
                            <td>{systemOutput[2][0]}</td>
                        </tr>
                        <tr>
                            <th>Y</th>
                            <td>{systemOutput[0][1]}</td>
                            <td>{systemOutput[1][1]}</td>
                            <td>{systemOutput[2][1]}</td>
                        </tr>
                        <tr>
                            <th>i</th>
                            <td>{systemOutput[0][2]}</td>
                            <td>{systemOutput[1][2]}</td>
                            <td>{systemOutput[2][2]}</td>
                        </tr>
                        <tr>
                            <th>1 уравнение</th>
                            <td>{this.props.systemEquation.check_f1(systemOutput[0][0],systemOutput[0][1])}</td>
                            <td>{this.props.systemEquation.check_f1(systemOutput[1][0],systemOutput[1][1])}</td>
                            <td>{this.props.systemEquation.check_f1(systemOutput[2][0],systemOutput[2][1])}</td>
                        </tr>
                        <tr>
                            <th>2 уравнение</th>
                            <td>{this.props.systemEquation.check_f2(systemOutput[0][0],systemOutput[0][1])}</td>
                            <td>{this.props.systemEquation.check_f2(systemOutput[1][0],systemOutput[1][1])}</td>
                            <td>{this.props.systemEquation.check_f2(systemOutput[2][0],systemOutput[2][1])}</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default SingleEquationTable;