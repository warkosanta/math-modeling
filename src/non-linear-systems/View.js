import Button from "@restart/ui/esm/Button";
import React, { Component } from "react";
import { Col, Container, Dropdown, DropdownButton, Form, FormControl, InputGroup, Row } from "react-bootstrap";
import SingleEquationTable from "./components/SingleEquationTable";
import VariantDescription from "./components/VariantDescription";
import equations from "./solvers/equations";
import systemEquation from "./solvers/systemEquation";

class View extends Component {
    constructor(props) {
        super(props);
        this.state = {
            variant: "",
            a: 1,
            b: 3,
            eps: 0.00001,
            x0: 1
        }
    }

    onVariantChange = (val) => {
        this.setState({ variant: val });
    }

    onInputChange = (val) => {
        this.setState({ [val.target.id]: parseFloat(val.target.value) })
    }
    onSubmit = (e) => {
        e.preventDefault();
    }

    render() {

        return (
            <Container>
                <h1>Лабораторная работа № 2</h1>
                <h2>Моделирование нелинейных систем</h2>
                <hr />
                <InputGroup className="mb-3">
                    <InputGroup.Text>A</InputGroup.Text>
                    <FormControl id="a" onChange={this.onInputChange} value={this.state.a} />
                    <InputGroup.Text>B</InputGroup.Text>
                    <FormControl id="b" onChange={this.onInputChange} value={this.state.b} />
                </InputGroup>
                <InputGroup className="mb-3">
                    <InputGroup.Text>x0</InputGroup.Text>
                    <FormControl id="x0" onChange={this.onInputChange} value={this.state.x0} />
                </InputGroup>
                <InputGroup className="mb-3">
                    <InputGroup.Text>Точность</InputGroup.Text>
                    <FormControl id="eps" onChange={this.onInputChange} value={this.state.eps} />
                    <DropdownButton title="Варианты" onSelect={this.onVariantChange}>
                        <Dropdown.Item eventKey="var23">Вариант 23 (Рамиля)</Dropdown.Item>
                        <Dropdown.Item eventKey="var8">Вариант 8 (Катя)</Dropdown.Item>
                        <Dropdown.Item eventKey="var12">Вариант 12 (Настя)</Dropdown.Item>
                    </DropdownButton>
                </InputGroup>
                {this.state.variant &&
                    <div>
                        <hr />
                        <VariantDescription variant={this.state.variant} />
                        <hr />
                        <SingleEquationTable {...this.state} equations={equations[this.state.variant]} systemEquation={systemEquation[this.state.variant]} />
                    </div>
                }
            </Container>
        );
    }
}

export default View;