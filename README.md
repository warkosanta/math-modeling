# Запуск

* Установите nodejs и npm глобально на вашу машину
* Скачайте репозиторий
* Перейдите в папку проекта, выполните команды:

    Установка пакетов:

    >npm install

    Запуск тестов:

    >npm test

    Запуск сервера:

    >npm start

    Просмотр:

    localhost:3000

![](images/img1.png)
![](images/img2.png)
![](images/img3.png)
![](images/img4.png)
![](images/img5.png)
![](images/img6.png)
![](images/img7.png)
